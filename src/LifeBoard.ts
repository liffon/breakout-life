import {Rectangle, overlapping} from './util';
import {parseLevel} from './parseLevel';

export type AnimationState = number|null;

export interface LifeBoardOptions {
  level: string,
  topLeft: Rectangle,
};

export class LifeBoard {
  constructor(options: LifeBoardOptions) {
      const parseResult = parseLevel(options.level);
      this.rows = parseResult.rows;
      this.cols = parseResult.cols;
      this._board = parseResult.board;
      this._animationBoard = parseResult.animationBoard;
      this.population = this._board
        .reduce(
          (carry, row) => carry + row.reduce(
              (carry, value) => carry + (value ? 1 : 0),
              0
            ),
          0
        );

    this.topLeft = options.topLeft;
    this.age = 0;
  }

  rows: number;
  cols: number;
  topLeft: Rectangle;
  padding = 2;

  _board: boolean[][];
  _animationBoard: AnimationState[][];
  age = 0;
  population = 0;

  get(row: number, col: number): boolean|undefined {
    if (
      row < 0 ||
      col < 0 ||
      row >= this.rows ||
      col >= this.cols
    ) {
      return undefined;
    }
    return this._board[row][col];
  }

  set(row: number, col: number, value: boolean): LifeBoard {
    const prevValue = this._board[row][col];
    if (prevValue !== value) {
      if (value) {
        this.population += 1;
      } else {
        this.population -= 1;
      }
      this._animationBoard[row][col] = 0;
    }
    this._board[row][col] = value;
    return this;
  }

  getRect() {
    return {
      x: this.topLeft.x,
      y: this.topLeft.y,
      width: this.topLeft.width * this.cols + this.padding * (this.cols - 1),
      height: this.topLeft.height * this.rows + this.padding * (this.rows - 1),
    };
  }

  checkCollisions(other: Rectangle): [number, number][] {
    let collisions: [number, number][] = [];

    let cell: Rectangle = Object.assign({}, this.topLeft);
    for (let row = 0; row < this.rows; ++row) {
      for (let col = 0; col < this.cols; ++col) {
        if (!this.get(row, col)) {
          continue;
        }
        cell.x = this.topLeft.x + col * (this.topLeft.width + this.padding);
        cell.y = this.topLeft.y + row * (this.topLeft.height + this.padding);
        if (overlapping(other, cell)) {
          collisions.push([row, col]);
        }
      }
    }

    return collisions;
  }

  step() {
    let births: [number, number][] = [];
    let deaths: [number, number][] = [];

    for (let row = 0; row < this.rows; ++row) {
      for (let col = 0; col < this.cols; ++col) {
        let neighbours = 0;
        neighbours += +(this.get(row - 1, col - 1) || false);
        neighbours += +(this.get(row - 1, col) || false);
        neighbours += +(this.get(row - 1, col + 1) || false);
        neighbours += +(this.get(row,      col - 1) || false);
        neighbours += +(this.get(row,      col + 1) || false);
        neighbours += +(this.get(row + 1, col - 1) || false);
        neighbours += +(this.get(row + 1, col) || false);
        neighbours += +(this.get(row + 1, col + 1) || false);

        if (this.get(row, col)) {
          if (neighbours < 2 || neighbours > 3) {
            deaths.push([row, col]);
          }
        } else if (neighbours === 3) {
          births.push([row, col]);
        }
      }
    }

    for (const [row, col] of births) {
      this.set(row, col, true);
    }
    for (const [row, col] of deaths) {
      this.set(row, col, false);
    }

    if (this.population > 0) {
      this.age += 1;
    }
  }

  onColor = '#000';
  offColor = '#eee';

  timeSinceLastAnimation = 0;
  render(ctx: CanvasRenderingContext2D, dt: number) {
    this.timeSinceLastAnimation += dt;

    if (this.timeSinceLastAnimation > 100) {
      this.timeSinceLastAnimation = 0;
      for (let row = 0; row < this.rows; ++row) {
        for (let col = 0; col < this.cols; ++col) {
          if (this._animationBoard[row][col] !== null) {
            this._animationBoard[row][col]! += 1;
            if (this._animationBoard[row][col] === 1) {
              this._animationBoard[row][col] = null;
            }
          }
        }
      }
    }
  
    for (let row = 0; row < this.rows; ++row) {
      for (let col = 0; col < this.cols; ++col) {
        const state = this.get(row, col);

        ctx.fillStyle = this.offColor;
        ctx.fillRect(
          this.topLeft.x + col * (this.topLeft.width + this.padding),
          this.topLeft.y + row * (this.topLeft.height + this.padding),
          this.topLeft.width,
          this.topLeft.height
        );
        ctx.fillStyle = state ? this.onColor : this.offColor;

        const animationState = this._animationBoard[row][col];
        if (animationState === null) {
          ctx.fillRect(
            this.topLeft.x + col * (this.topLeft.width + this.padding),
            this.topLeft.y + row * (this.topLeft.height + this.padding),
            this.topLeft.width,
            this.topLeft.height
          );
        } else if (animationState === 0) {
          ctx.fillRect(
            this.topLeft.x + col * (this.topLeft.width + this.padding) + this.topLeft.width /4,
            this.topLeft.y + row * (this.topLeft.height + this.padding) + this.topLeft.height /4,
            this.topLeft.width / 2,
            this.topLeft.height / 2
          );
        }
      }
    }
  }
};

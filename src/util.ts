export class Rectangle {
  x: number;
  y: number;
  width: number;
  height: number;
}

export interface Velocity {
  vx: number,
  vy: number,
}

export function clamp(value: number, lowerBound = -Infinity, upperBound = Infinity) {
  return Math.min(Math.max(lowerBound, value), upperBound);
}

export function overlapping(r1: Rectangle, r2: Rectangle) {
  const [r1x1, r1y1, r1x2, r1y2] = [r1.x, r1.y, r1.x + r1.width, r1.y + r1.height];
  const [r2x1, r2y1, r2x2, r2y2] = [r2.x, r2.y, r2.x + r2.width, r2.y + r2.height];

  const overlapX = r1x2 > r2x1 && r2x2 > r1x1;
  const overlapY = r1y2 > r2y1 && r2y2 > r1y1;
  return overlapX && overlapY;
}

export function setVelocity(v: Velocity|number, angle: number): Velocity {
  const speed = typeof v === 'number' ? v : Math.sqrt(v.vx * v.vx + v.vy * v.vy);
  return {
    vx: speed * Math.cos(angle),
    vy: -speed * Math.sin(angle),
  };
}
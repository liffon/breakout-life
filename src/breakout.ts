import {LifeBoard} from './LifeBoard';
import {Rectangle, Velocity, clamp, overlapping, setVelocity} from './util';

document.body.onload = () => {
    const urlParams = new URLSearchParams(window.location.search);
    let app = new App(document, urlParams.get('level'));

    function updateAndRender(time: number) {
        app.updateAndRender(time);
        requestAnimationFrame(updateAndRender);
    }
    requestAnimationFrame(updateAndRender);
};

class App {
    context: CanvasRenderingContext2D;
    scoreField: HTMLElement;

    constructor(document: Document, level: string|null = null) {
        const canvas = <HTMLCanvasElement> document.getElementById('canvas');
        canvas.width = 320;
        canvas.height = 440;
        this.context = canvas.getContext('2d')!;
        [this.width, this.height] = [canvas.width, canvas.height];
        this.clientRect = canvas.getBoundingClientRect();

        level = level || `12,16;#_
#__#____________
____#___________
#___#___________
_####___________
________________
________________
________________
________________
___________####_
___________#___#
___________#____
____________#__#
`;

        this.board = new LifeBoard({
            topLeft: {x: 17, y: 17, width: 16, height: 16},
            level: level
        });

        this.scoreField = document.getElementById('score')!;
        this.scoreField.innerHTML = '' + this.board.age;

        document.body.onblur = () => { this.blur(); };
        document.body.onfocus = () => { this.focus(); };

        document.body.onmousemove = (event: MouseEvent) => { this.mousemove(event); };
        document.body.onclick = (event: MouseEvent) => { this.mouseclick(event); };

        document.body.ontouchmove = (event: TouchEvent) => {
            let touch = event.touches[0];
            document.body.dispatchEvent(new MouseEvent(
                'mousemove',
                {
                    clientX: touch.clientX,
                    clientY: touch.clientY,
                }
            ))
        };
        
        document.body.onkeydown = (event: KeyboardEvent) => { this.keydown(event); };
    }

    width: number;
    height: number;
    clientRect: ClientRect;
    lastTime: number|null = null;
    lag = 0;
    timeSinceLastHit = Infinity;
    shouldPropagate = false;

    running = false;
    gameOver = false;

    ball: Rectangle&Velocity = {
        x: 164, y: 323,
        width: 10, height: 10,
        ...setVelocity({vx: 0.2, vy: 0}, -Math.PI / 2.18)
    }
    paddle: Rectangle = {
        x: 0, y: 440 - 24,
        width: 60, height: 14,
    };

    gameOverText: Rectangle = {
        x: 160, y: 0,
        width: 0, height: 0,
    }
    gameOverAnimationStarted = false;

    board: LifeBoard;

    blur() {
        this.running = false;
    }

    focus() {
        this.lastTime = null;
    }

    mousemove(event: MouseEvent) {
        let [canvasX, canvasY] = [
            event.clientX - this.clientRect.left,
            event.clientY - this.clientRect.top,
        ];
        if (false) console.log({canvasX, canvasY});
        this.paddle.x = clamp(canvasX - this.paddle.width / 2, 0, this.width - this.paddle.width);
    }

    mouseclick(event: MouseEvent) {
        if (!this.running && !this.gameOver) {
            this.running = true;
        }
    }

    keydown(event: KeyboardEvent) {
        if (event.code === 'Space') {
            this.board.step();
        }
    }

    updateAndRender(time: number) {
        if (this.lastTime === null) {
            this.lastTime = time;
            return;
        }
        this.lag += time - this.lastTime;
        const timeDiffForFrame = time - this.lastTime;
        this.lastTime = time;

        this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);

        const dt = 1000/60;
        let hitCell = false;
        while (this.lag >= dt) {
            this.lag -= dt;
            const [ball, paddle] = [this.ball, this.paddle];
            if (this.running && !this.gameOver) {
                ball.x += ball.vx * dt;
                
                if (ball.x > this.width - ball.width || ball.x < 0) {
                    ball.vx *= -1;
                    ball.x += ball.vx * dt;
                } else if (overlapping(ball, paddle)) {
                    // TODO: Prevent player from haxing by quick-dragging paddle into ball?
                    ball.vx *= -1;
                    ball.x += ball.vx * dt;
                } else if (overlapping(ball, this.board.getRect())) {
                    const collisions = this.board.checkCollisions(ball);
                    if (collisions.length > 0) {
                        for (let [row, col] of collisions) {
                            this.board.set(row, col, false);
                        }
                        ball.vx *= -1;
                        ball.x += ball.vx * dt;
                        hitCell = true;
                    }
                }

                ball.y += ball.vy * dt;
                if (ball.y < 0) {
                    ball.vy *= -1;
                    ball.y += ball.vy * dt;
                } else if(ball.y > this.height) {
                    this.running = false;
                    this.gameOver = true;
                } else if (overlapping(ball, paddle)) {
                    ball.vy *= -1;
                    ball.y += ball.vy * dt;

                    const middleAngle = Math.PI / 2;

                    let velocity: Velocity;
                    if (ball.x + ball.width / 2 < paddle.x + ball.width / 4) {
                        velocity = setVelocity(ball, middleAngle + Math.PI * 0.4);
                    } else if (ball.x + ball.width / 2 < paddle.x + paddle.width / 4) {
                        velocity = setVelocity(ball, middleAngle + Math.PI * 0.2);
                    } else if (ball.x + ball.width / 2 < paddle.x + 2 * paddle.width / 4) {
                        velocity = setVelocity(ball, middleAngle + Math.PI * 0.1);
                    } else if (ball.x + ball.width / 2 < paddle.x + 3 * paddle.width / 4) {
                        velocity = setVelocity(ball, middleAngle - Math.PI * 0.1);
                    } else if (ball.x + ball.width / 2 < paddle.x + paddle.width - ball.width / 4) {
                        velocity = setVelocity(ball, middleAngle - Math.PI * 0.2);
                    } else {
                        velocity = setVelocity(ball, middleAngle - Math.PI * 0.4);
                    }
                    [ball.vx, ball.vy] = [velocity.vx, velocity.vy];
                } else if (overlapping(ball, this.board.getRect())) {
                    const collisions = this.board.checkCollisions(ball);
                    if (collisions.length > 0) {
                        for (let [row, col] of collisions) {
                            this.board.set(row, col, false);
                        }
                        ball.vy *= -1;
                        ball.y += ball.vy * dt;
                        hitCell = true;
                    }
                }

                if (hitCell) {
                    this.timeSinceLastHit = 0;
                    this.shouldPropagate = true;
                }

                if (this.shouldPropagate && this.timeSinceLastHit > 100) {
                    this.shouldPropagate = false;
                    this.board.step();
                    this.scoreField.innerHTML = '' + this.board.age;
                    if (this.board.population === 0) {
                        this.gameOver = true;
                        this.running = false;
                    }
                }

                this.timeSinceLastHit += dt;
            } else if (this.gameOver) {
                ball.y = this.height + 10;
                ball.vx = ball.vy = 0;

                const boardRect = this.board.getRect();                
                const goalY = (boardRect.y + boardRect.height + 30);
                if (Math.abs(goalY - this.gameOverText.y) > 5) {
                    this.gameOverText.y =
                        (60 * this.gameOverText.y + goalY) / 61;
                }
                this.gameOverAnimationStarted = true;
            }
        }

        this.board.render(this.context, timeDiffForFrame);

        this.context.fillStyle = '#000';
        const [ball, paddle] = [this.ball, this.paddle];

        this.context.fillStyle = '#555';
        this.context.fillRect(ball.x, ball.y, ball.width, ball.height);
        this.context.fillRect(paddle.x, paddle.y, paddle.width, paddle.height);

        if (this.gameOver) {
            this.context.fillStyle = '#555';
            this.context.textAlign = 'center';
            this.context.font = '24px sans-serif';
            this.context.fillText('Game Over', this.gameOverText.x, this.gameOverText.y);
        }
    };
}

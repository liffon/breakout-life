import {AnimationState} from './LifeBoard';

interface ParseLevelResult {
  rows: number,
  cols: number,
  board: boolean[][],
  animationBoard: AnimationState[][],
}

export function parseLevel(level: string): ParseLevelResult {
  let rows: number, cols: number;

  let it = 0;
  [rows, it] = consumeNumber(level, it);
  it = consumeLiteral(level, it, ',');
  [cols, it] = consumeNumber(level, it);
  it = consumeLiteral(level, it, ';');

  let onChar: string, offChar: string;
  [onChar, it] = consumeChar(level, it);
  [offChar, it] = consumeChar(level, it);

  let board = new Array(rows)
    .fill(undefined)
    .map(() => new Array(cols).fill(false));

  let animationBoard = new Array(rows)
    .fill(undefined)
    .map(() => new Array(cols).fill(false));

  for (let row = 0; row < rows; ++row) {
    it = consumeMaybeLiteralChar(level, it, '\n');
    for (let col = 0; col < cols; ++col) {
      if (typeof level[it] === 'undefined') {
        throw new ParseError('Level input is too short (ended at row ' + row + ', col ' + col + ')');
      }
      let char;
      [char, it] = consumeChar(level, it);
      if (char === onChar) {
        board[row][col] = true;
      } else if (char === offChar) {
        board[row][col] = false;
      } else {
        throw new ParseError('Expected on (`' + onChar + '`) or off (`' + offChar + '`), got `' + char + '`');
      }
    }    
  }

  return {
    rows,
    cols,
    board,
    animationBoard,
  };
}

export class ParseError extends Error {}

function consumeLiteral(input: string, it: number, value: string): number {
  if (input.slice(it).startsWith(value)) {
    return it + value.length;
  } else {
    throw new ParseError('Could not match literal `' + value + '`, found `' + input[it] + '`');
  }
}

function consumeNumber(input: string, it: number): [number, number] {
  let numberString = '';
  while (input[it] >= '0' && input[it] <= '9' && it < input.length) {
    numberString += input[it++];
  }
  if (numberString.length === 0) {
    throw new ParseError('Expected number, got ' + input[it]);
  } else {
    return [Number(numberString), it];
  }
}

function consumeChar(input: string, it: number): [string, number] {
  return [input[it], it + 1];
}

function consumeMaybeLiteralChar(input: string, it: number, value: string): number {
  if (input[it] === value) {
    ++it;
  }
  return it;
}

function peek(input: string, it: number): string {
  return input[it];
}

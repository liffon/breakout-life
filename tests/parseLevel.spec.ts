import {parseLevel, ParseError} from '../src/parseLevel'

test('simple 2x3 board', () => {
  let {rows, cols, board, animationBoard} = parseLevel(`2,3;*.
**.
.*.
`);
  expect(rows).toBe(2);
  expect(cols).toBe(3);
  expect(board.length).toBe(rows);
  board.forEach(row => expect(row.length).toBe(cols));
  [[0,0], [0,1], [1,1]]
    .forEach(([row, col]) => expect(board[row][col]).toBe(true));
});

test('simple 6x8 board', () => {
  let {rows, cols, board, animationBoard} = parseLevel(`6,8;o-
------o-
--------
----o---
--------
------o-
-o-----o
`);
  expect(rows).toBe(6);
  expect(cols).toBe(8);
  expect(board.length).toBe(rows);
  board.forEach(row => expect(row.length).toBe(cols));
  [[0,6], [2,4], [4,6], [5,1], [5,7]]
    .forEach(([row, col]) => expect(board[row][col]).toBe(true));
});

test('too short input', () => {
	expect(() => parseLevel('14,7;yaaaaaaaayyyyyyyaa'))
	  .toThrow(new ParseError('Level input is too short (ended at row 2, col 2)'));
});